// Cách cũ -- function monAn(){

// }
export class MonAn {
  constructor(ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa) {
    this.ma = ma;
    this.ten = ten;
    this.loai = loai;
    this.giaMon = giaMon;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
  }
  tinhGiaKhuyenMai = function () {
    return this.giaMon * (1 - this.khuyenMai / 100);
  };
}
