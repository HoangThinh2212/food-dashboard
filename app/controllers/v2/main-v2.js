import { deleteFoodByID, getAllFood } from "../../service/service.js";
import { renderFoodList } from "./controller-v2.js";

let fetchAllFoodService = () => {
  getAllFood()
    .then((res) => {
      renderFoodList(res.data);
      console.log("res: ", res);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
fetchAllFoodService();

function xoaMonAn(id) {
  deleteFoodByID(id)
    .then((res) => {
      Swal.fire("Xóa món ăn thành công");
      fetchAllFoodService();
    })
    .catch((err) => {
      console.log("eSrr: ", err);
      Swal.fire({
        icon: "error",
        title: "Noooo...",
        text: "Đồ ăn không có nữa rùi",
        // footer: '<a href="">Tại seooooo?</a>'
      });
    });
}
// window là Đối tượng có sẵn ~ dùng để hỗ trợ định nghĩa các file trong Module
window.xoaMonAn = xoaMonAn;
