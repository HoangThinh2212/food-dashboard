export const renderFoodList = (list) => {
  let contentHMTL = "";
  list.forEach((item) => {
    let { ten, loai, phanTramKM, gia, tinhTrang, ma } = item;
    contentHMTL += `<tr>
        <td>${ma}</td>
        <td>${ten}</td>
        <td>${loai? "Mặn":"Chay"}</td>
        <td>${gia}</td>
        <td>${phanTramKM} %</td>
        <td>0</td>
        <td>${tinhTrang ? "Còn" : "Hết"}</td>
        <td>
            <button onclick="xoaMonAn(${ma})" class="btn btn-danger">Xóa</button>
            <button class="btn btn-warning">Sửa</button>
        </td>
    </tr>`;
  });
  document.getElementById("tbodyFood").innerHTML = contentHMTL;
};
