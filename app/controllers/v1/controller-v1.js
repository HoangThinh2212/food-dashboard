// Lấy thông tin từ Form
export function layThongTinTuForm() {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  return {
    ma,
    ten,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}
// Hiển thị món ăn
// 1 file chỉ dc export default 1 lần
export default function showThongTinLenForm(data) {
  let { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } = data;
  document.getElementById("imgMonAn").src = hinhMon;
  document.getElementById("spMa").innerText = ma;
  document.getElementById("spTenMon").innerText = ten;
  document.getElementById("spLoaiMon").innerText = loai;
  document.getElementById("spGia").innerText = giaMon;
  document.getElementById("spKM").innerText = khuyenMai;
  document.getElementById("spTT").innerText = tinhTrang;
  document.getElementById("pMoTa").innerText = moTa;
  document.getElementById("spGiaKM").innerText = data.tinhGiaKhuyenMai();
}
