import { layThongTinTuForm } from "./controller-v1.js";
//     tên biến tự đặt
import hienThiThongTin from "./controller-v1.js";
import { MonAn } from "../../models/v1/monAnModel-v1.js";
// addEventListener sự kiện khi ng dùng click vào ~ khi gán vào sự kiện onclick bên HTML

//Thêm món ăn
document.getElementById("btnThemMon").addEventListener("click", function () {
  let data = layThongTinTuForm();
  // let monAn = new MonAn(
  //   data.ma,
  //   data.ten,
  //   data.loai,
  //   data.giaMon,
  //   data.khuyenMai,
  //   data.tinhTrang,
  //   data.hinhMon,
  //   data.moTa
  // );

  let { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } = data;
  let monAn = new MonAn(
    ma,
    ten,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  hienThiThongTin(monAn);
});
