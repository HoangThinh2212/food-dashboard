let BASE_URL = "https://6367801cf5f549f052d68fd3.mockapi.io";

export let getAllFood = () => {
  return axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  });
};

export let deleteFoodByID = (id) => {
  return axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  });
};
